package com.cathayholdings.beans.dependencyinjection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeansDependencyInjectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeansDependencyInjectionApplication.class, args);
	}

}
