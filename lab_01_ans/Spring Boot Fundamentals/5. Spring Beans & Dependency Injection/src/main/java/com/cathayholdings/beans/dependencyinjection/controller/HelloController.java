package com.cathayholdings.beans.dependencyinjection.controller;

import com.cathayholdings.beans.dependencyinjection.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * 以下三種注入方法，擇一方法實現即可 */
@RestController
public class HelloController {

    // 注入方法一: field
    @Autowired
    private HelloService helloService;

      // 注入方法二: Setter
//    private HelloService helloService;
//    @Autowired
//    public void setHelloService(HelloService helloService) {
//        this.helloService = helloService;
//    }

    // 注入方法三: Constructor
//    private HelloService helloService;
//    public HelloController(HelloService helloService) {
//        this.helloService = helloService;
//    }

    @GetMapping("/hello")
    public String sayHello() {
        return helloService.hello();
    }
}
