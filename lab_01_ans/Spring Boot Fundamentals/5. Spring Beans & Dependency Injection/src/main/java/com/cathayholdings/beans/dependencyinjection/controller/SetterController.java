package com.cathayholdings.beans.dependencyinjection.controller;

import com.cathayholdings.beans.dependencyinjection.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // 跟Spring告知要管理這個Class
public class SetterController {

    // 需注入的物件
    private DemoService demoService;

    @Autowired // 被注入物件的Setter方法
    public void setDemoService(DemoService demoService){
       this.demoService = demoService;
    }

    @GetMapping("setter")
    public String injectType() {
        return demoService.say("Setter");
    }
}
