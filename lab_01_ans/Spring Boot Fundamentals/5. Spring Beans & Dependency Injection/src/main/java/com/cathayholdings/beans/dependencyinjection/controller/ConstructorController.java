package com.cathayholdings.beans.dependencyinjection.controller;

import com.cathayholdings.beans.dependencyinjection.service.DemoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // 跟Spring告知要管理這個Class
public class ConstructorController {

    // 需注入物件
    private DemoService demoService;

    // constructor方式注入物件
    public ConstructorController(DemoService demoService) {
        this.demoService = demoService;
    }

    @GetMapping("constructor")
    public String injectType() {
        return demoService.say("Constructor");
    }
}
