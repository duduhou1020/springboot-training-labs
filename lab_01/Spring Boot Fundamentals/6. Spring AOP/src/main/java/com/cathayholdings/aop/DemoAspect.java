package com.cathayholdings.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect // 告知Spring這是一個Aspect
@Component // 告知Spring要管理這一個Class
public class DemoAspect {

    // 針對com.cathayholdings.aop.controller.AopDemoController裡的所有方法
    @Pointcut("execution(* com.cathayholdings.aop.controller.AopDemoController.*(..))")
    public void controllerLog() {
    }

    @Before("controllerLog()") // 使用@Pointcut定義好的方法
    public void doBefore(){
        System.out.println("AOP doBefore!!");
    }

    @After("controllerLog()") // 使用@Pointcut定義好的方法
    public void doAfter(){
        System.out.println("AOP doAfter@@");
    }
}
