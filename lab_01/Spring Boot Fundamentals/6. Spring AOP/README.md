# Spring AOP

> AOP(Aspect-Oriented Programming，面相切面程式設計)

AOP的目的在於解耦，可以讓同一組class共享相同行為
主要可以讓你的業務邏輯去關注自己本身的業務，而不去想一些其他的事情，這些其他的事情包括：安全，事務，日誌等這種共同行為，便可以透過AOP來處理。

* @Aspect：聲明此class是一個切面
* @After、@Before、@Around：參數可建立攔截規則，也就是切點（PointCut）
* @PointCut：定義攔截規則（可以重複使用）
* 符合條件的被攔截處，稱為連接點（JoinPoint）
* 攔截分為兩種
    1. 註解攔截
    2. 方法攔截

**引入AOP**

build.gradle
```groovy
implementation 'org.springframework.boot:spring-boot-starter-aop' // 引入Spring boot aop
```
**DemoAspect.java**
```java
@Aspect // 告知Spring這是一個Aspect
@Component // 告知Spring要管理這一個Class
public class DemoAspect {

    // 針對com.cathayholdings.aop.controller.AopDemoController裡的所有方法
    @Pointcut("execution(* com.cathayholdings.aop.controller.AopDemoController.*(..))")
    public void controllerLog() {
    }

    @Before("controllerLog()") // 使用@Pointcut定義好的方法
    public void doBefore(){
        System.out.println("AOP doBefore!!");
    }

    @After("controllerLog()") // 使用@Pointcut定義好的方法
    public void doAfter(){
        System.out.println("AOP doAfter@@");
    }
}
```

**AopDemoController.java**
```java
@RestController
public class AopDemoController {

    @GetMapping("aop")
    public String aopDemo(){
        System.out.println("This is /aop EndPoint");
        return "This AOP Return Message";
    }
}
```

**測試**

服務啟動後，在瀏覽器上輸入以下網址

[http://localhost:8080/aop](http://localhost:8080/aop)

查看IDE console結果
![Imgur image](https://imgur.com/JSctvCM.png)

以上我們利用Spring Boot AOP創建了自定義批註，我們可以將其應用於Spring bean，以便在運行時向其註入額外的行為。
