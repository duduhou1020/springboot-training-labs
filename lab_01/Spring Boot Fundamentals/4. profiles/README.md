# Porfiles

> 一個應用為了在不同的環境下工作，常常會有不同的配置，程式碼邏輯處理(ex: DB連線)。
> Spring Boot 對此提供了簡便的支援。

## 區分環境的配置
### YAML檔配置

假設我們應用程式工作環境有：dev、test、prod
那麼我們可以新增個種配置檔案:
* application.yml      - 公用配置
* application-dev.yml  - 開發環境配置
* application-test.yml - 測試環境配置
* application-prod.yml - 生產環境配置

## 啟動Profile方式
### 配置檔案啟用<br>

在application.yml檔案中可以通過以下配置來啟動Profile:

```yaml
spring:
  profiles:
    active: dev
```
以上表示當前環境啟動application-dev.yml檔案中的配置

## 測試

* application.yml
```yaml
spring:
  profiles:
    active: dev # 設定當前環境為何(dev,test,prod)
```

* application-dev.yml
```yaml
server:
  port: 8081 # 設定dev環境的port為8081

demo:
  profile:
    string: "This is Development Profile!!" # 自定義文字,測試判斷哪個環境使用.
```
* application-test.yml
```yaml
server:
  port: 8082

demo:
  profile:
    string: "This is Test Profile!!" # 自定義文字,測試判斷哪個環境使用
```

* application-prod.yml
```yaml
server:
  port: 8080

demo:
  profile:
    string: "This is Production Profile!!" # 自定義文字,測試判斷哪個環境使用
```
* SpringBootDemoProfilesApplication.java
```java
@SpringBootApplication
@RestController
public class SpringBootDemoProfilesApplication {

	@Value("${demo.profile.string}") // @Value: 從配置文件讀取值。
	private String profile;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDemoProfilesApplication.class, args);
	}

	@GetMapping("/profile")
	public String profile(){
		return profile; // 返回從配置文件中的Profile文字
	}
}
```

## 測試結果
1. application.yml - spring.profiles.active=dev

![Imgur image](https://i.imgur.com/rMyosav.png)

2. application.yml - spring.profiles.active=test

![Imgur image](https://i.imgur.com/SG3ARCg.png)

3. application.yml - spring.profiles.active=prod

![Imgur image](https://imgur.com/DUc8QL4.png)

## 結論
從以上測試結果得知，Spring Boot有良好的支援在多個環境中需要不同的配置檔。
